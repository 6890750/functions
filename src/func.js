const getSum = (str1, str2) => {
  if(str1==='')
  {
    str1=0;
  }else if(typeof str1 === 'object'|| str1.length==0 || str1.replace(/[^a-z]/g, '').length >0 || 
  typeof str2 === 'object'|| str2.length==0 || str2.replace(/[^a-z]/g, '').length >0)
  {
    return false;
  }
  if(str2==='')
  {
    str2=0;
  }
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let p=0,c=0;
  listOfPosts.forEach(element => {
    if(element.author == authorName)
      p++;
    if(element.hasOwnProperty('comments'))
      element.comments.forEach(function (el) { if (el.author == authorName) c++;} );
  });
  return `Post:${p},comments:${c}`;
};

const tickets=(people)=> {
  let kassa=0;
  for (let element of people) {
    if (element - (kassa + 25) > 0) {
      return 'NO';
    }
    kassa += 25 - (element-25);
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
